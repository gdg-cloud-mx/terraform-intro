provider "google" {
  credentials = file("gcp-terraform-250514-e355f1bc6eb9.json")

  project = "gcp-terraform-250514"
  region  = "us-central1"
  zone    = "us-central1-c"
}

resource "google_compute_network" "vpc_network" {
  name = "terraform-network"
}
