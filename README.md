# Terraform Introduction

Terraform is an open-source infrastructure as code software tool created by HashiCorp.

## Installation

Follow instructions on how to install at https://learn.hashicorp.com/terraform/gcp/install

## Usage
```terraform
terraform init

terraform apply

terraform show
```

## Contributing

Pull requests are welcome.

